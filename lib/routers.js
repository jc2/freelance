Router.configure({
  layoutTemplate: 'cfd',
  
});
Router.map(function() {
  this.route('projectShow', {
    path: '/projects/:_id',
    data: function () {
      return Projects.findOne(this.params._id);
    },
    action: function () {
      //this.render('projectShow', {to: 'projects'});
      this.render();
    }
  });

  this.route('home', {
    path: '/',
/*    action: function() {
      Router.go('projectShow', Projects.findOne());
    }*/
  });
  this.route('mySkills', {
    path: '/myskills/',
  });
  this.route('myKnowledge', {
    path: '/myknowledge/',
  });

  this.route('createProject');
});