Tasks = new Mongo.Collection("tasks");
Requests= new Mongo.Collection("request");
Projects = new Mongo.Collection("projects");
Skills = new Mongo.Collection("skills");
Knowledges = new Mongo.Collection("knowledges");

Uploads = new FS.Collection("uploads", {
  stores: [new FS.Store.FileSystem('upload', {path: './uploads'})]
});
