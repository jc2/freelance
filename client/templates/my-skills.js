if (Meteor.isClient) {
	Template.mySkills.helpers({
	  skillsIWant: function () {
	    return Skills.find({checked: {$in:[false,null]}}, {sort: {createdAt: -1}});
	  },
	  skillsIKnow: function () {
	    return Skills.find({checked: true}, {sort: {createdAt: -1}});
	  },
	});

	Template.mySkills.events({
	  "submit .new-skill": function (event) {
	    var text = event.target.text.value;
	    if(text){
	      Skills.insert({
	        text: text,
	        createdAt: new Date() 
	      });
	      event.target.text.value = "";
	    }
	    return false;
	  },
	});
}