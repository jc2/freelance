Meteor.methods({
  delete_uploads: function(activity){
    Uploads.remove({activity: activity});
  }
});

if (Meteor.isClient) {

	Template.request.events({
	  "click .toggle-checked": function () {
	      Requests.update(this._id, {$set: {checked: ! this.checked}}
	      );
	  },
	  "click .delete-task": function () {
	  	Meteor.call('delete_uploads', this._id);
	    Requests.remove(this._id);
	  },
	  "change .file-input": function (event){
	      var activity = this._id;
	      FS.Utility.eachFile(event, function(file){
	        var upload = new FS.File(file);
	        upload.activity = activity;
	        Uploads.insert(upload, function(err){
	          console.log(err);
	        });
	      });
	      return false;
	    },
	    "click .delete-file": function (){
	      Uploads.remove(this._id);
	    },
	});
	Template.request.helpers({
		uploads: function () {
      		return Uploads.find({activity: this._id});
    	}
	});
	
}