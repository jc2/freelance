if (Meteor.isClient) {

	Template.task.events({
	  "click .toggle-checked": function () {
	      Tasks.update(this._id, {$set: {startTime: null,
	                                     duration: this.checked ? 0:60,
	                                     checked: ! this.checked,
	                                     finishTime: this.checked ? null: new Date()
	                                    }
	                              }
	      );
	  },
	  "click .delete-task": function () {
	    Tasks.remove(this._id);
	  },
	  "click .play-task": function () {
	    Tasks.update(this._id, {$set: {startTime: new Date() }});
	  },
	  "click .stop-task": function () {
	      var now = new Date();
	      var newDuration = (now -  this.startTime)/1000/60 | 0
	      Tasks.update(this._id, {$set: {startTime: null,
	                                     duration: newDuration,
	                                     checked: newDuration==0  ? false:true,
	                                     finishTime: new Date()
	                                    }
	                              }
	      );
	  },
	});
	
}