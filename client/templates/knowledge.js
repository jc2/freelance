if (Meteor.isClient) {

	Template.knowledge.events({
	  "click .delete-task": function () {
	    Knowledges.remove(this._id);
	  },
	  "click .toggle-private": function(){
	  	Knowledges.update(this._id, {$set: {isPrivate: ! this.isPrivate}}
	      );
	  }
	});
	
}