if (Meteor.isClient) {
	Template.projectShow.helpers({
	  tasks: function () {
	    return Tasks.find({projectId: this._id}, {sort: {createdAt: -1}});
	  },
	  requests: function () {
	    return Requests.find({projectId: this._id}, {sort: {createdAt: -1}});
	  },
	  projectName: function(){
	    return this.name;
	  },
	  deadline: function(){
	    return this.deadline.toISOString().substring(0, 10);
	  },
	  projectEffort: function(){
	  	
	  	return totalEffort;
	  },
	  //optimizar esto
	  projectDetails: function(){
	  	var totalTodos = Tasks.find({projectId: this._id}).count();
	  	var currentDones = Tasks.find({projectId: this._id, checked: true}).count();

	  	var totalEffort = 0;
	  	Tasks.find({projectId: this._id, checked:true}).map(function(doc){
	  		totalEffort += doc.duration;
	  	});

	  	var currentCost = totalEffort/60*50000;

	  	var project = Projects.findOne(this._id);
	  	var now = new Date();

	  	var daysElapsed = (now-project.createdAt)/1000/60/60/24 | 0;
	  	var daysLeft = (project.deadline-now)/1000/60/60/24 | 0;

	  	if (daysElapsed != 0){
	  		var avgDoneUpToNow = currentDones/daysElapsed;
	  	}
	  	else{
	  		var avgDoneUpToNow = currentDones;
	  	}
	  	
	  	if(daysLeft != 0){
	  		var avgStillNotDone = (totalTodos-currentDones)/daysLeft;
	  	}
	  	else{
	  		var avgStillNotDone = 0;
	  	}
	  	
	  	// var totalDays = (project.deadline-project.createdAt)/1000/60/60/24 | 0;
	  	// var startDay = project.deadline;
	  	// var days = new Array();
	  	// for (var i = 0;i<totalDays ;i++){
	  	// 	days.push(startDay);
	  	// 	startDay.setDate(startDay.getDate() + 1);
	  	// }
	  	// console.log(days);
	  	var costWarning = false;
	  	if (currentCost >= project.cost){
	  		costWarning = true;
	  	}

	  	var performanceAdvice = false;
  		if (avgDoneUpToNow < avgStillNotDone){
  			performanceAdvice = true;
  		}

	  	return {
	  		totalTodos: totalTodos,
  			currentDones: currentDones,
  			totalEffort: totalEffort,
  			currentCost: currentCost.formatMoney(0, '.', '.'),
  			daysElapsed: daysElapsed,
  			daysLeft: daysLeft,
  			progress:100*currentDones/totalTodos | 0,
  			avgDoneUpToNow: avgDoneUpToNow.toFixed(2),
  			avgStillNotDone: avgStillNotDone.toFixed(2),
  			costWarning: costWarning,
  			performanceAdvice: performanceAdvice,
	  		};
	  }
	});

	Template.projectShow.events({
	  "submit .new-task": function (event) {
	    var text = event.target.text.value;
	    if(text){
	      Tasks.insert({
	        projectId: this._id,
	        text: text,
	        createdAt: new Date() 
	      });
	      event.target.text.value = "";
	    }

	    return false;
	  },
	  "submit .new-request": function (event) {
	    var text = event.target.text.value;
	    if(text){
	      Requests.insert({
	        projectId: this._id,
	        text: text,
	        createdAt: new Date() 
	      });
	      event.target.text.value = "";
	    }

	    return false;
	  },
	  "submit .edit-project": function(event){
	    var name = event.target.name.value;
	    var cost = event.target.cost.value;
	    var deadline = event.target.deadline.value;
	    var skills = event.target.skills.value;
	    if (name && cost && deadline && skills){
		    Projects.update(this._id, {$set: {
		        name: name,
		        cost: cost,
		        deadline: new Date(deadline),
		        skills: skills
		    }});
		}
	    return false;
	  },
	  "click .delete-project": function(){
	    Projects.remove(this._id);
	    Router.go('home');
	  }
	});

	Template.projectShow.rendered=function() {
	  $('#datepicker').datepicker({
	    format: "yyyy-mm-dd"
		});
	}
}