if (Meteor.isClient) {
	Template.myKnowledge.helpers({
	  otro: function(){
	  	return Knowledges.find({},{sort: {createdAt: -1}});
	  }
	});

	Template.myKnowledge.events({
	  "submit .new-knowledge": function (event) {
	    var name = event.target.name.value;
	    var content = $("#content").html();
	    var skills = event.target.skills.value;
	    if(name){
	      Knowledges.insert({
	        name: name,
	        content: content,
	        skills: skills,
	        createdAt: new Date()
	      });
	      event.target.name.value = "";
	      $("#content").html("");
	      event.target.skills.value = "";
	    }
	    return false;
	  },
	});
}