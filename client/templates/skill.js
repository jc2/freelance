if (Meteor.isClient) {

	Template.skill.events({
	  "click .toggle-checked": function () {
	      Skills.update(this._id, {$set: {checked: ! this.checked,
	      								 isPrivate: ! this.isPrivate}}
	      );
	  },
	  "click .delete-task": function () {
	    Skills.remove(this._id);
	  },
	  "click .toggle-private": function(){
	  	Skills.update(this._id, {$set: {isPrivate: ! this.isPrivate}}
	      );
	  }
	});
	
}