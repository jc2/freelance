if (Meteor.isClient) {

	Template.createProject.events({
    "submit .new-project": function (event) {
      var name = event.target.name.value;
      var cost = event.target.cost.value;
      var deadline = event.target.deadline.value;
      var skills = event.target.skills.value;
      if (name && cost && deadline && skills){
        var newProject = {
          name: name,
          cost: cost,
          deadline: new Date(deadline),
          skills: skills,
          createdAt: new Date()
        }
        newProject._id = Projects.insert(newProject);
        Router.go('projectShow', newProject);
      }
      return false;

      // event.target.name.value = "";
      // event.target.cost.value = "";
      // event.target.deadline.value = "";
    },
	});

	Template.createProject.rendered=function() {
    $('#datepicker').datepicker({
      format: "yyyy-mm-dd"
    });
  }

}